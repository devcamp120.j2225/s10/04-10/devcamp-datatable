import { CHANGE_PAGE_PAGINATION } from "../constants/paginationActionTypes";

const initialState = {
    currentPage: 1
}

export default function paginationReducers(state = initialState, action) {
    switch (action.type) {
        case CHANGE_PAGE_PAGINATION:
            return {
                ...state,
                currentPage: action.payload
            }
        default:
            return state;
    }
}